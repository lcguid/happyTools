# This file is part of happyTools.
#
# happyTools. is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# happyTools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with happyTools.  If not, see <https://www.gnu.org/licenses/>.

#' @export
#' @title Source external R scripts while checking if the file exists.
#'
#' @description
#' Receives a string that corresponds to a directory (\code{dir_}) where the
#' script is located, and the file name for the script (\code{lib_name_}). It
#' then makes sure the file exists. If it doesn't, it issues a warning with an
#' error message.
#'
#' @param dir_ The directory where the script is located
#' @param path_ The path to the script from the directory

#' @return Nothing is returned. The file is sourced.

#' @author Aidan Fahlman <\email{aidanfahlman@@gmail.com}>

#' @examples
#' get_lib( 'R/misc/', 'my_R_script.R' )


get_lib = function(
  dir_,
  lib_name_
)
{
  .Deprecated(
    'source_file',
    package = 'happyTools',
    old = as.character(
      sys.call( sys.parent() )
    )[1L]
  )
  source(	happyTools::set_file_name( paste( sep = '/', dir_, lib_name_)	)	)
}
