# This file is part of happyTools.
#
# happyTools. is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# happyTools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with happyTools.  If not, see <https://www.gnu.org/licenses/>.

#' @export
#' @title Format dates into (YYYYMMDD)
#' 
#' @description
#' This function will format your \code{date_list_} into YYYYMMDD from the
#' existing format provided in \code{original_format_}
#'
#' @param date_list_ the date you need to have formatted
#' @param originial_format_ the format of the date provided
#'
#' @return This function will return a formatted date as (YYYYMMDD)
#'
#' @author Luiz Guidolin <\email{lm.guidolin@@gmail.com}>
#'
#' @examples
#' my_date = format_date(Sys.time(), "%Y-%m-%d")
#'	 - This example gets and formats the system time
#' my_date = format_date("19850112", "%Y%d%m")
#' 	 - This example is formatting a YYYYDDMM date
#' my_date = format_date("18_12_25", "19%y_m_d")
#'	 - This example shows the need for "19" if you are formatting a (YY)
#'	   date instead of a (YYYY) date to avoid confusion between centuries

format_date <- function(
  date_list_,
  original_format_
)
{
  format( as.Date( date_list_, original_format_, tz = "UTC" ), "%Y%m%d" )
}